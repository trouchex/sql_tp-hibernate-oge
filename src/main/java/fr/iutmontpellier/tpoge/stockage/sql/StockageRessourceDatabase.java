package fr.iutmontpellier.tpoge.stockage.sql;

import fr.iutmontpellier.tpoge.metier.entite.Ressource;
import fr.iutmontpellier.tpoge.stockage.Stockage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StockageRessourceDatabase implements Stockage<Ressource> {



    @Override
    public void create(Ressource element) {
        /*
    Fait une requête d’insertion sur la base en utilisant les attributs de l’entité passée
    en paramètre vous n’aurez pas à gérer l’identifiant de l’entité car celui-ci s’incrémente
    automatiquement (géré du côté de la base). Pour une ressource, il faut donc seulement
    insérer un nom.
     */

        Connection connection = SQLUtils.getInstance().getConnection();
            String requete = "INSERT INTO ressourcesOGE (nom) VALUES (?)";

        try (
                PreparedStatement statement = connection.prepareStatement(requete);
                ) {
            // executeIpdate c pour INSERT, UPDATE, DELETE
            // executeQuery c pour SELECT

            statement.setString(1, element.getNom());

            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Ressource element) {
        Connection connection = SQLUtils.getInstance().getConnection();
        // requete dois gerer les injections sql (pour eviter les attaques)
        String requete = "UPDATE ressourcesOGE SET nom = ? WHERE idRessource = ?";
        System.out.println(requete);

        try {
            PreparedStatement statement = connection.prepareStatement(requete);
            statement.setString(1, element.getNom());
            statement.setInt(2, element.getIdRessource());

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteById(int id) {
        Connection connection = SQLUtils.getInstance().getConnection();

        // la requete doit gerer les injections sql (pour eviter les attaques)
String requete = "DELETE FROM ressourcesOGE WHERE idRessource = ?";

        try {
            PreparedStatement statement = connection.prepareStatement(requete);
            statement.setInt(1, id);

            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Ressource getById(int id) {
        Connection connection = SQLUtils.getInstance().getConnection();
        String requete = "SELECT * FROM ressourcesOGE WHERE idRessource = ?";
        Ressource ressource = new Ressource();

        try {
            PreparedStatement statement = connection.prepareStatement(requete);
            statement.setInt(1, id);

            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            ressource.setIdRessource(resultSet.getInt("idRessource"));
            ressource.setNom(resultSet.getString("nom"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ressource;
    }

    @Override
    public List<Ressource> getAll() {
        Connection connection = SQLUtils.getInstance().getConnection();
        String requete = "SELECT * FROM ressourcesOGE";
        List<Ressource> listeR = new ArrayList<>();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(requete);
            while (resultSet.next()) {
                Ressource ressource = new Ressource();
                ressource.setIdRessource(resultSet.getInt("idRessource"));
                ressource.setNom(resultSet.getString("nom"));
                listeR.add(ressource);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listeR;
    }
}
