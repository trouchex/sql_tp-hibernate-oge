package fr.iutmontpellier.tpoge;

import fr.iutmontpellier.tpoge.ihm.stage.GestionStage;
import javafx.application.Application;
import javafx.stage.Stage;

/*
Stript a éxécuter avant lancement de l'application
 */

/*
DROP TABLE RessourcesOGE CASCADE CONSTRAINT;
DROP TABLE EtudiantsOGE CASCADE CONSTRAINT;
DROP TABLE NotesOGE CASCADE CONSTRAINT;
DROP SEQUENCE ressources_oge_autoincrement;
DROP SEQUENCE etudiants_oge_autoincrement;
DROP SEQUENCE notes_oge_autoincrement;
 */

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        new GestionStage();
    }

    //https://gitlabinfo.iutmontp.univ-montp2.fr/trouchex/sql_tp-oge.git


    public static void main(String[] args) {
        launch();
    }
}
